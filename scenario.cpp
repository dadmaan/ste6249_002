﻿
#include <iostream>

#include "scenario.h"
#include "computation.h"


#include <parametrics/surfaces/gmpplane.h>


// hidmanager
#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>

// qt
#include <QQuickItem>
//    std::shared_ptr<ComputationModel> _computation;
ComputationModel computation;
GMlib::PathTrack* line;


template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}




void Scenario::initializeScenario() {

  lynxMotionController(0,500,200);

  // Insert a light
  GMlib::Point<GLfloat,3> init_light_pos( 2.0, 4.0, 10 );
  GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                     GMlib::GMcolor::white(), init_light_pos );
  light->setAttenuation(0.8f, 0.002f, 0.0008f);
  this->scene()->insertLight( light, false );

  // Insert Sun
  this->scene()->insertSun();

  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3>  init_cam_pos( 5.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 1.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_up(  1.0f, 0.0f, 0.0f );

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 8000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(-45), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, -20.0f, 20.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );


  /***************************************************************************
   *                                                                         *
   * Standar example, including path track and path track arrows             *
   *                                                                         *
   ***************************************************************************/

//  GMlib::Material mm(GMlib::GMmaterial::polishedBronze());
//  mm.set(45.0);

  //Basement - Small Cylinder
  bc = new TestCylinder(0.25f, 0.25f, 0.5f);
  bc->setMaterial(GMlib::GMmaterial::jade());
  bc->current_angle = M_PI/2;
  bc->rotate( GMlib::Angle(bc->current_angle), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f) );
  bc->move(GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f), true);
  bc->toggleDefaultVisualizer();
  bc->sample(60,60,1,1);
  this->scene()->insert(bc);

  //First Joint - Sphere 1
  js1 = new TestSphere(0.36f);
  js1->setMaterial(GMlib::GMmaterial::blackPlastic());
  js1->move(GMlib::Vector<float,3>(0.0f, 0.0f, 0.5f), true);
  js1->current_angle = -30*M_PI/180;
  js1->rotate( GMlib::Angle(js1->current_angle), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
  js1->toggleDefaultVisualizer();
  js1->sample(60,60,1,1);
  bc->insert(js1);

  //First Arm - Cylinder 2
  link1 = new TestCylinder(0.25f, 0.25f, 1.8f);
  link1->setMaterial(GMlib::GMmaterial::jade());
  link1->move(GMlib::Vector<float,3>(0.0f,0.0f,1.15f), true);
  link1->toggleDefaultVisualizer();
  link1->sample(60,60,1,1);
  js1->insert(link1);

  //Second Joint - Sphere 2
  js2 = new TestSphere(0.36f);
  js2->setMaterial(GMlib::GMmaterial::blackPlastic());
  js2->move(GMlib::Vector<float,3>(0.0f, 0.0f, 0.9f), true);
  js2->current_angle = 60*M_PI/180;
  js2->rotate( GMlib::Angle(js2->current_angle), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
  js2->toggleDefaultVisualizer();
  js2->sample(60,60,1,1);
  link1->insert(js2);

  //Second Arm - Cylinder 3
  link2 = new TestCylinder(0.25f, 0.25f, 1.8f);
  link2->setMaterial(GMlib::GMmaterial::jade());
  link2->move(GMlib::Vector<float,3>(0.0f,0.0f,1.15f), true);
  link2->toggleDefaultVisualizer();
  link2->sample(60,60,1,1);
  js2->insert(link2);

  //Third Joint - Sphere 3
  js3 = new TestSphere(0.36f);
  js3->setMaterial(GMlib::GMmaterial::blackPlastic());
  js3->move(GMlib::Vector<float,3>(0.0f, 0.0f, 0.9f), true);
  js3->current_angle = 15*M_PI/180;
  js3->rotate( GMlib::Angle(js3->current_angle), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
  js3->toggleDefaultVisualizer();
  js3->sample(60,60,1,1);
  link2->insert(js3);

  //Second Arm - Cylinder 3
  link3 = new TestCylinder(0.25f, 0.25f, 0.25f);
  link3->setMaterial(GMlib::GMmaterial::jade());
  link3->move(GMlib::Vector<float,3>(0.0f,0.0f,0.36f), true);
  link3->toggleDefaultVisualizer();
  link3->sample(60,60,1,1);
  js3->insert(link3);

  //Anchor - Cylinder 4
  pen_body = new TestCylinder(0.05f, 0.05f, 4.0f);
  pen_body->setMaterial(GMlib::GMmaterial::polishedRed());
  pen_body->rotate(GMlib::Angle(M_PI/2), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f) );
  pen_body->move(GMlib::Vector<float,3>(1.0f,0.0f,0.05f), true);
  pen_body->toggleDefaultVisualizer();
  pen_body->sample(60,60,1,1);
  link3->insert(pen_body);

  //Third Joint - Sphere 3
  pen_ball = new TestSphere(0.05f);
  pen_ball->setMaterial(GMlib::GMmaterial::polishedBronze());
  pen_ball->move(GMlib::Vector<float,3>(0.0f, 0.0f, 2.0f), true);
  pen_ball->toggleDefaultVisualizer();
  pen_ball->sample(60,60,1,1);
  pen_body->insert(pen_ball);

  //Drawing Line
  line = new GMlib::PathTrack();
  line->setLineWidth(2);
  line->move(GMlib::Vector<float,3>(0.05f, 0.05f, 2.0f), true);
  pen_body->insert(line);
  line->setVisible(false);


//  auto debug2 = new TestSphere(0.5f);
//  debug2->setMaterial(GMlib::GMmaterial::ruby());
//  debug2->move(GMlib::Vector<float,3>(0.415349f, 0.0f, -2.0f), true);
//  debug2->toggleDefaultVisualizer();
//  debug2->sample(60,60,1,1);
//  this->scene()->insert(debug2);

//  auto debug1 = new TestSphere(0.5f);
//  debug1->setMaterial(GMlib::GMmaterial::ruby());
//  debug1->move(GMlib::Vector<float,3>(0.415349f, -0.0f, -4.0f), true);
//  debug1->toggleDefaultVisualizer();
//  debug1->sample(60,60,1,1);
//  this->scene()->insert(debug1);

//  debug = new TestSphere(0.5f);
//  debug->setMaterial(GMlib::GMmaterial::ruby());
//  debug->move(GMlib::Vector<float,3>(0.415349f, -2.0f, -4.0f), true);
//  debug->toggleDefaultVisualizer();
//  debug->sample(60,60,1,1);
//  this->scene()->insert(debug);

//  auto debug3 = new TestSphere(0.5f);
//  debug3->setMaterial(GMlib::GMmaterial::ruby());
//  debug3->move(GMlib::Vector<float,3>(0.415349f, -2.0f, -2.0f), true);
//  debug3->toggleDefaultVisualizer();
//  debug3->sample(60,60,1,1);
//  this->scene()->insert(debug3);
}

void Scenario::cleanupScenario() {

}

void Scenario::rotate(double sv, char s)
{
    switch(s){
    case 0:bc->rotate( GMlib::Angle(sv), computation.baseRotAxis);
        break;
    case 1:js1->rotate( GMlib::Angle(sv), computation.jointRotAxis);
        break;
    case 2:js2->rotate( GMlib::Angle(sv), computation.jointRotAxis);
        break;
    case 3:js3->rotate( GMlib::Angle(sv), computation.jointRotAxis);
        break;
    }
}

double Scenario::desirAngle(double sv, TestSphere *j)
{
    auto ca = j->current_angle;
    auto da = sv - ca;
    j->current_angle = sv;
    return da;
}
double Scenario::desirAngle(double sv, TestCylinder *b)
{
    auto ca = b->current_angle;
    auto da = sv - ca;
    b->current_angle = sv;
    return da;
}


void Scenario::armController()
{
    auto _targetPos = this->targetPos;
        computation.init(bc,link1,link2,link3,pen_body,
                         js1,js2,js3,pen_ball,false);
        auto Angles = computation.computeInverseKinematic(_targetPos);

        auto da = desirAngle(double(Angles[0]),bc);
        bc->rotate_flag = true;
        bc->rotation_angle = da;
//            bc->rotate(GMlib::Angle(da),computation.baseRotAxis);

        da = desirAngle(double(Angles[1]),js1);
        js1->rotate_flag = true;
        js1->rotation_angle = da;
//            js1->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(double(Angles[2]),js2);
        js2->rotate_flag = true;
        js2->rotation_angle = da;
//            js2->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(double(Angles[3]),js3);
        js3->rotate_flag = true;
        js3->rotation_angle = da;
//            js3->rotate(GMlib::Angle(da),computation.jointRotAxis);

}

void Scenario::setTargetPos()
{
    this->targetPos = pen_ball->getGlobalPos();
    std::cout<<"Position: " << this->targetPos << std::endl;
}

void Scenario::lynxMotionController(int joint, int angle, int velocity)
{
    if(joint == 3){
        auto j = "#" + std::to_string(joint);
        auto v = "S" + std::to_string(velocity);
        if(angle >= 160)
            auto a = "P" + std::to_string(1778+(angle*2.93));
        else if(angle <= 20)
            auto a = "P" + std::to_string(600+(angle*6));
        else {
            auto a = "P" + std::to_string(500+(angle*1.111111111));
        }
    }
    auto j = "#" + std::to_string(joint);
    auto a = "P" + std::to_string(2500-(angle*1.111111111));
    auto v = "S" + std::to_string(velocity);


    _lynxmotion.setPortName("/dev/ttyUSB0");
    _lynxmotion.setBaudRate(QSerialPort::Baud115200, QSerialPort::AllDirections);
    _lynxmotion.open(QIODevice::ReadWrite);
    if (!_lynxmotion.open(QIODevice::ReadWrite))
    {
        std::cout << "serial port could not be opened"<<std::endl;
        std::cout << _lynxmotion.error()<<std::endl;
    }

//    const QByteArray responseData = QByteArray::fromStdString(j+a+v+"\r");
    const QByteArray responseData = "#0 P500 S200 \r";
    _lynxmotion.write(responseData);
}

void Scenario::initDraw()
{
    auto angle = desirAngle(0.35749,bc);
    bc->rotate_flag = true;
    bc->rotation_angle = angle;

    angle = desirAngle(0.486667,js1);
    js1->rotate_flag = true;
    js1->rotation_angle = angle;

    angle = desirAngle(1.01109,js2);
    js2->rotate_flag = true;
    js2->rotation_angle = angle;

    angle = desirAngle(-0.579773,js3);
    js3->rotate_flag = true;
    js3->rotation_angle = angle;
}

void Scenario::drawSqaure()
{
    line->setVisible(true);
    if(node1){
        auto da = desirAngle(1.57006,bc);
        bc->rotate_flag = true;
        bc->rotation_angle = da;
        //            bc->rotate(GMlib::Angle(da),computation.baseRotAxis);

        da = desirAngle(-0.29475,js1);
        js1->rotate_flag = true;
        js1->rotation_angle = da;
        //            js1->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(1.32301,js2);
        js2->rotate_flag = true;
        js2->rotation_angle = da;
        //            js2->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(0.389053,js3);
        js3->rotate_flag = true;
        js3->rotation_angle = da;
        js3->rotate(GMlib::Angle(da),computation.jointRotAxis);
        node1 = false;
        node2 = true;
    }
    if(node2){
        auto da = desirAngle(1.56998,bc);
        bc->rotate_flag = true;
        bc->rotation_angle = da;
        //            bc->rotate(GMlib::Angle(da),computation.baseRotAxis);

        da = desirAngle(0.184365,js1);
        js1->rotate_flag = true;
        js1->rotation_angle = da;
        //            js1->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(1.16429,js2);
        js2->rotate_flag = true;
        js2->rotation_angle = da;
        //            js2->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(-0.217225,js3);
        js3->rotate_flag = true;
        js3->rotation_angle = da;
        //            js3->rotate(GMlib::Angle(da),computation.jointRotAxis);
        node2 = false;
    }

    if(node3){
        auto da = desirAngle(1.09937,bc);
        bc->rotate_flag = true;
        bc->rotation_angle = da;
        //            bc->rotate(GMlib::Angle(da),computation.baseRotAxis);

        da = desirAngle(0.352527,js1);
        js1->rotate_flag = true;
        js1->rotation_angle = da;
        //            js1->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(1.07017,js2);
        js2->rotate_flag = true;
        js2->rotation_angle = da;
        //            js2->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(-0.428913,js3);
        js3->rotate_flag = true;
        js3->rotation_angle = da;
        //            js3->rotate(GMlib::Angle(da),computation.jointRotAxis);
        node3 = false;
    }

    if(node4){
        auto da = desirAngle(0.864938,bc);
        bc->rotate_flag = true;
        bc->rotation_angle = da;
        //            bc->rotate(GMlib::Angle(da),computation.baseRotAxis);

        da = desirAngle(-0.1063,js1);
        js1->rotate_flag = true;
        js1->rotation_angle = da;
        //            js1->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(1.34525,js2);
        js2->rotate_flag = true;
        js2->rotation_angle = da;
        //            js2->rotate(GMlib::Angle(da),computation.jointRotAxis);

        da = desirAngle(0.191662,js3);
        js3->rotate_flag = true;
        js3->rotation_angle = da;
        //            js3->rotate(GMlib::Angle(da),computation.jointRotAxis);
        node4 = false;
    }
    //1st Position: \0.439795 \-1.33113e-34 \-2.0346
    //Angles: \1.57006 \-0.29475 \1.32301 \0.389053

    //2nd Position: 0.53873 -2.60682e-34 -3.98444
    //Angles: 1.56998 0.184365 1.16429 -0.217225

    //3rd Position: 0.351389 -2.58775 -3.83658
    // Angles: 1.09937 0.352527 1.07017 -0.428913

    //4th Position: 0.506361 -1.90133 -2.03897
    // Angles: 0.864938 -0.1063 1.34525 0.191662
}

void Scenario::drawCircle()
{
    line->setVisible(true);
    bc->rotate_flag = true;
    bc->rotation_angle = 2*M_PI;
}

void Scenario::defaultPos()
{
    line->setVisible(false);
    auto angle = desirAngle(M_PI/2, bc);
    bc->rotate_flag = true;
    bc->rotation_angle = angle;

    angle = desirAngle(-30*M_PI/180, js1);
    js1->rotate_flag = true;
    js1->rotation_angle = angle;

    angle = desirAngle(60*M_PI/180, js2);
    js2->rotate_flag = true;
    js2->rotation_angle = angle;

    angle = desirAngle(15*M_PI/180, js3);
    js3->rotate_flag = true;
    js3->rotation_angle = angle;

    std::cout<<"Model Reset"<< std::endl;
}

void Scenario::delay(int number_of_seconds)
{
    int milli_seconds = 1000 * number_of_seconds;
    clock_t start_time = clock();
    while (clock() < start_time + milli_seconds);
}

void Scenario::callDefferedGL() {

  GMlib::Array< const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
    if(e_obj(i)->isVisible()) e_obj[i]->replot();
}

