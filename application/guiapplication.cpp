#include "guiapplication.h"

// local
#include "window.h"
#include "gmlibwrapper.h"

// hidmanager
#include "../hidmanager/defaulthidmanager.h"
#include "../hidmanager/hidmanagertreemodel.h"


// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QStringListModel>
#include <QOpenGLContext>

// stl
#include <cassert>


std::unique_ptr<GuiApplication> GuiApplication::_instance {nullptr};


GuiApplication::GuiApplication(int& argc, char **argv) : QGuiApplication(argc, argv) {

  assert(!_instance);
  _instance = std::unique_ptr<GuiApplication>(this);


  qRegisterMetaType<HidInputEvent::HidInputParams> ("HidInputEvent::HidInputParams");

  connect( &_window, &Window::sceneGraphInitialized,
           this,     &GuiApplication::onSceneGraphInitialized,
           Qt::DirectConnection );

  connect( &_window, &Window::sceneGraphInvalidated,
           this,     &GuiApplication::onSceneGraphInvalidated,
           Qt::DirectConnection );

  connect( this, &GuiApplication::signOnSceneGraphInitializedDone,
           this, &GuiApplication::afterOnSceneGraphInitialized );

  connect( this, &QGuiApplication::lastWindowClosed,
           this, &QGuiApplication::quit );

  _window.rootContext()->setContextProperty( "rc_name_model", &_scenario.rcNameModel() );
  _window.rootContext()->setContextProperty( "hidmanager_model", _hidmanager.getModel() );
  _window.setSource(QUrl("qrc:///qml/main.qml"));

  _window.show();
}

GuiApplication::~GuiApplication() {

  _scenario.stop();
  _window.setPersistentOpenGLContext(false);
  _window.setPersistentSceneGraph(false);
  _window.releaseResources();
  _instance.release();

  qDebug() << "Bye bye ^^, ~~ \"emerge --oneshot life\"";
}

void
GuiApplication::onSceneGraphInitialized() {

  qDebug() << "GL context: " << QOpenGLContext::currentContext()->format();

  // Init GMlibWrapper
  _scenario.initialize();
  _hidmanager.init(_scenario);
  connect( &_scenario,  &GMlibWrapper::signFrameReady,
           &_window,    &Window::update );

  // Init test scene of the GMlib wrapper
  _scenario.initializeScenario();
  _scenario.prepare();

  emit signOnSceneGraphInitializedDone();
}

void GuiApplication::onSceneGraphInvalidated() {

  _scenario.cleanUp();
}

void
GuiApplication::afterOnSceneGraphInitialized() {

  // Hidmanager setup
  _hidmanager.setupDefaultHidBindings();
  connect( &_window, &Window::signKeyPressed,         &_hidmanager, &StandardHidManager::registerKeyPressEvent );
  connect( &_window, &Window::signKeyReleased,        &_hidmanager, &StandardHidManager::registerKeyReleaseEvent );
  connect( &_window, &Window::signMouseDoubleClicked, &_hidmanager, &StandardHidManager::registerMouseDoubleClickEvent);
  connect( &_window, &Window::signMouseMoved,         &_hidmanager, &StandardHidManager::registerMouseMoveEvent );
  connect( &_window, &Window::signMousePressed,       &_hidmanager, &StandardHidManager::registerMousePressEvent );
  connect( &_window, &Window::signMouseReleased,      &_hidmanager, &StandardHidManager::registerMouseReleaseEvent );
  connect( &_window, &Window::signWheelEventOccurred, &_hidmanager, &StandardHidManager::registerWheelEvent );

  // Handle HID OpenGL actions; needs to have the OGL context bound;
  // QQuickWindow's beforeRendering singnal provides that on a DirectConnection
  connect( &_window, &Window::beforeRendering,        &_hidmanager, &DefaultHidManager::triggerOGLActions,
           Qt::DirectConnection );

  // Register an application close event in the hidmanager;
  // the QWindow must be closed instead of the application being quitted,
  // this is to make sure that GL exits gracefully
  QString ha_id_var_close_app =
  _hidmanager.registerHidAction( "Application", "Quit", "Close application!", &_window, SLOT(close()));
  _hidmanager.registerHidMapping( ha_id_var_close_app, new KeyPressInput( Qt::Key_Q, Qt::ControlModifier) );

  // Connect some application spesific inputs.
  connect( &_hidmanager, &DefaultHidManager::signToggleSimulation,
           &_scenario,   &GMlibWrapper::toggleSimulation );

  connect( &_hidmanager,          SIGNAL(signOpenCloseHidHelp()),
           _window.rootObject(),  SIGNAL(toggleHidBindView()) );

  // Update RCPair name model
  _scenario.updateRCPairNameModel();

  // Start simulator
  _scenario.start();


  connect( &_window, &Window::beforeRendering, &_scenario, &Scenario::callDefferedGL, Qt::DirectConnection );

  //Signals From Sliders
  connect( _window.rootObject(), SIGNAL(sliderControllerBase(int)),
           this, SLOT(sliderControllerBase(int)));

  connect( _window.rootObject(), SIGNAL(sliderControllerJoint1(int)),
           this, SLOT(sliderControllerJoint1(int)));

  connect( _window.rootObject(), SIGNAL(sliderControllerJoint2(int)),
           this, SLOT(sliderControllerJoint2(int)));

  connect( _window.rootObject(), SIGNAL(sliderControllerJoint3(int)),
           this, SLOT(sliderControllerJoint3(int)));

  connect( _window.rootObject(), SIGNAL(moveArm()),
           this, SLOT(moveArm()));

  connect( _window.rootObject(), SIGNAL(targetPos()),
           this, SLOT(targetPos()));

  connect( _window.rootObject(), SIGNAL(resetDefault()),
           this, SLOT(resetDefault()));

  connect( _window.rootObject(), SIGNAL(drawCircle()),
           this, SLOT(drawCircle()));

  connect( _window.rootObject(), SIGNAL(drawSquare()),
           this, SLOT(drawSquare()));

  connect( _window.rootObject(), SIGNAL(setSatrtingPosition()),
           this, SLOT(setSatrtingPosition()));
}

void GuiApplication::sliderControllerBase(int s0)
{
    auto angle = _scenario.desirAngle(s0*M_PI/180, _scenario.bc);
    _scenario.rotate(angle, 0);
    _scenario.lynxMotionController(0,angle,200);
}

void GuiApplication::sliderControllerJoint1(int s1)
{
    auto angle = _scenario.desirAngle(s1*M_PI/180, _scenario.js1);
    _scenario.rotate(angle, 1);
    _scenario.lynxMotionController(1,angle,200);
}

void GuiApplication::sliderControllerJoint2(int s2)
{
    auto angle = _scenario.desirAngle(s2*M_PI/180, _scenario.js2);
    _scenario.rotate(angle, 2);
    _scenario.lynxMotionController(2,angle,200);
}

void GuiApplication::sliderControllerJoint3(int s3)
{
    auto angle = _scenario.desirAngle(s3*M_PI/180, _scenario.js3);
    _scenario.rotate(angle, 3);
    _scenario.lynxMotionController(3,angle,200);
}

void GuiApplication::moveArm()
{
    _scenario.armController();
}

void GuiApplication::targetPos()
{
    _scenario.setTargetPos();
}

void GuiApplication::resetDefault()
{
    _scenario.defaultPos();
}

void GuiApplication::drawSquare()
{
    _scenario.drawSqaure();
}

void GuiApplication::drawCircle()
{
    _scenario.drawCircle();
}

void GuiApplication::setSatrtingPosition()
{
    _scenario.initDraw();
}

const GuiApplication& GuiApplication::instance() {  return *_instance; }
