import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.5

import "qrc:/qml/components"

import SceneGraphRendering 1.0

Item {
  id: root

  signal toggleHidBindView
  signal sliderControllerBase(int s0)
  signal sliderControllerJoint1(int s1)
  signal sliderControllerJoint2(int s2)
  signal sliderControllerJoint3(int s3)
  signal moveArm()
  signal targetPos()
  signal resetDefault()
  signal setSatrtingPosition()
  signal drawCircle()
  signal drawSquare()

  onToggleHidBindView: hid_bind_view.toggle()

  Renderer {
    id: renderer

    anchors.fill: parent

    rcpair_name: rc_pair_cb.currentText

    ComboBox {
      id: rc_pair_cb
      anchors.top: parent.top
      anchors.left: parent.left
      anchors.margins: 5

      width: 128

      opacity: 0.7

      model: rc_name_model
      textRole: "display"
      currentIndex: 0
    }

    Button {
      text: "?"
      anchors.top: parent.top
      anchors.right: parent.right
      anchors.margins: 5

      width: height
      opacity: 0.7

      onClicked: hid_bind_view.toggle()
    }

    HidBindingView {
      id: hid_bind_view
      anchors.fill: parent
      anchors.margins: 50
      visible:false

      states: [
        State{
          name: "show"
          PropertyChanges {
            target: hid_bind_view
            visible: true
          }
        }
      ]

      function toggle() {

        if(state === "") state = "show"
        else state = ""

      }
    }


    //********************************** Sliders **********************************
    //*****************************************************************************
    Rectangle {
        width: 400
        height: 600
        color: "#000000"
        radius: 15

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 150
        anchors.leftMargin: 5

//        property Slider slider1_value: slider1.value
//        property Slider slider2_value: slider2.value
//        property Slider slider3_value: slider3.value

        Label{
            color: "#ffffff"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 20
            anchors.leftMargin: 45

            font.pixelSize: 16
            text: 'Base Cotroller'
            horizontalAlignment: Text.AlignHCenter
        }
        Slider {
            id: slider0
            property int base_angle: 0
            from: -90
            to: 90
            snapMode: Slider.SnapOnRelease
            value: base_angle

            anchors.top: parent.top
            anchors.topMargin: 50

            background: Rectangle {
                x: slider0.leftPadding
                y: slider0.topPadding + slider0.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: slider0.availableWidth
                height: implicitHeight
                radius: 2
                color: "#bdbebf"

                Rectangle {
                    width: slider0.visualPosition * parent.width
                    height: parent.height
                    color: "#21be2b"
                    radius: 2
                }
            }

            handle: Rectangle {
                x: slider0.leftPadding + slider0.visualPosition * (slider0.availableWidth - width)
                y: slider0.topPadding + slider0.availableHeight / 2 - height / 2
                implicitWidth: 26
                implicitHeight: 26
                radius: 13
                color: slider0.pressed ? "#f0f0f0" : "#f6f6f6"
                border.color: "#bdbebf"
            }
            onValueChanged: sliderControllerBase(slider0.value)
        }

        Label{
            color: "#ffffff"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 100
            anchors.leftMargin: 30

            font.pixelSize: 16
            text: 'Fisrt Joint Cotroller'
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Slider {
            id: slider1
            property int joint1_angle: 0
            from: -90
            to: 90
            snapMode: Slider.SnapOnRelease
            value: joint1_angle

            anchors.top: parent.top
            anchors.topMargin: 130

            background: Rectangle {
                x: slider1.leftPadding
                y: slider1.topPadding + slider1.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: slider1.availableWidth
                height: implicitHeight
                radius: 2
                color: "#bdbebf"

                Rectangle {
                    width: slider1.visualPosition * parent.width
                    height: parent.height
                    color: "#21be2b"
                    radius: 2
                }
            }

            handle: Rectangle {
                x: slider1.leftPadding + slider1.visualPosition * (slider1.availableWidth - width)
                y: slider1.topPadding + slider1.availableHeight / 2 - height / 2
                implicitWidth: 26
                implicitHeight: 26
                radius: 13
                color: slider1.pressed ? "#f0f0f0" : "#f6f6f6"
                border.color: "#bdbebf"
            }
            onValueChanged: sliderControllerJoint1(slider1.value)
        }

        Label{
            color: "#ffffff"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 180
            anchors.leftMargin: 20

            font.pixelSize: 16
            text: 'Second Joint Cotroller'
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Slider {
            id: slider2
            property int joint2_angle: 0
            from: -90
            to: 90
            snapMode: Slider.SnapOnRelease
            value: joint2_angle

            anchors.top: parent.top
            anchors.topMargin: 220

            background: Rectangle {
                x: slider2.leftPadding
                y: slider2.topPadding + slider2.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: slider2.availableWidth
                height: implicitHeight
                radius: 2
                color: "#bdbebf"

                Rectangle {
                    width: slider2.visualPosition * parent.width
                    height: parent.height
                    color: "#21be2b"
                    radius: 2
                }
            }

            handle: Rectangle {
                x: slider2.leftPadding + slider2.visualPosition * (slider2.availableWidth - width)
                y: slider2.topPadding + slider2.availableHeight / 2 - height / 2
                implicitWidth: 26
                implicitHeight: 26
                radius: 13
                color: slider2.pressed ? "#f0f0f0" : "#f6f6f6"
                border.color: "#bdbebf"
            }
            onValueChanged: sliderControllerJoint2(slider2.value)
        }

        Label{
            color: "#ffffff"
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.topMargin: 270
            anchors.leftMargin: 30

            font.pixelSize: 16
            text: 'Third Joint Cotroller'
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Slider {
            id: slider3
            property int joint3_angle: 0
            from: -90
            to: 90
            snapMode: Slider.SnapOnRelease
            value: joint3_angle

            anchors.top: parent.top
            anchors.topMargin: 310

            background: Rectangle {
                x: slider3.leftPadding
                y: slider3.topPadding + slider3.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 4
                width: slider3.availableWidth
                height: implicitHeight
                radius: 2
                color: "#bdbebf"

                Rectangle {
                    width: slider3.visualPosition * parent.width
                    height: parent.height
                    color: "#21be2b"
                    radius: 2
                }
            }

            handle: Rectangle {
                x: slider3.leftPadding + slider3.visualPosition * (slider3.availableWidth - width)
                y: slider3.topPadding + slider3.availableHeight / 2 - height / 2
                implicitWidth: 26
                implicitHeight: 26
                radius: 13
                color: slider3.pressed ? "#f0f0f0" : "#f6f6f6"
                border.color: "#bdbebf"
            }
            onValueChanged: sliderControllerJoint3(slider3.value)
        }
    Rectangle
    {
        width: 150
        height: 60
        color: "#000000"
        anchors.top: parent.top
        anchors.topMargin: 370
        anchors.left: parent.left
        anchors.leftMargin: 10

        Button {
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 20
            text: "Set Position"
            onClicked: targetPos()
        }
        Button {
            anchors.left: parent.left
            anchors.leftMargin: 120
            anchors.top: parent.top
            anchors.topMargin: 20
            text: "Reset"
            onClicked: resetDefault()
        }
        Button {
            anchors.left: parent.left
            anchors.leftMargin: 60
            anchors.top: parent.top
            anchors.topMargin: 70
            text: "I Feel Lucky!!"
            onClicked: moveArm()
        }
    }
    Rectangle {
        width: 140
        height: 400
        color: "#000000"
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 240

        Button {
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.top: parent.top
            anchors.topMargin: 20
            text: "Ready!"
            onClicked: setSatrtingPosition()
        }
        Button {
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.top: parent.top
            anchors.topMargin: 70
            text: "Draw Square"
            onClicked: drawSquare()
        }
        Button {
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.top: parent.top
            anchors.topMargin: 120
            text: "Draw Circle"
            onClicked: drawCircle()
        }
    }
        }
    //****************************** End of Sliders *******************************
    //*****************************************************************************



//    ControlSlider {
//        anchors.top: parent.top
//        anchors.left: parent.left
//        anchors.topMargin: 150
//        anchors.leftMargin: 20
//        visible: true
//    }

  }
}
