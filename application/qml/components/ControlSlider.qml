import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.5

Rectangle {
    width: 200
    height: 300
    color: "#000000"
    radius: 15

    property Slider slider1_value: slider1.value
    property Slider slider2_value: slider2.value
    property Slider slider3_value: slider3.value

    Label{
        color: "#ffffff"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 20
        anchors.leftMargin: 30

        font.pixelSize: 16
        text: 'First Joint Cotroller'
        horizontalAlignment: Text.AlignHCenter
    }
    Slider {
        id: slider0
        from: -90
        to: 90
        snapMode: Slider.SnapOnRelease
        value: 0

        anchors.top: parent.top
        anchors.topMargin: 50

        background: Rectangle {
            x: slider0.leftPadding
            y: slider0.topPadding + slider0.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: slider0.availableWidth
            height: implicitHeight
            radius: 2
            color: "#bdbebf"

            Rectangle {
                width: slider0.visualPosition * parent.width
                height: parent.height
                color: "#21be2b"
                radius: 2
            }
        }

        handle: Rectangle {
            x: slider0.leftPadding + slider0.visualPosition * (slider0.availableWidth - width)
            y: slider0.topPadding + slider0.availableHeight / 2 - height / 2
            implicitWidth: 26
            implicitHeight: 26
            radius: 13
            color: slider0.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
    }

    Label{
        color: "#ffffff"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 100
        anchors.leftMargin: 20

        font.pixelSize: 16
        text: 'Second Joint Cotroller'
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Slider {
        id: slider1
        from: -90
        to: 90
        snapMode: Slider.SnapOnRelease
        value: 0

        anchors.top: parent.top
        anchors.topMargin: 130

        background: Rectangle {
            x: slider1.leftPadding
            y: slider1.topPadding + slider1.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: slider1.availableWidth
            height: implicitHeight
            radius: 2
            color: "#bdbebf"

            Rectangle {
                width: slider1.visualPosition * parent.width
                height: parent.height
                color: "#21be2b"
                radius: 2
            }
        }

        handle: Rectangle {
            x: slider1.leftPadding + slider1.visualPosition * (slider1.availableWidth - width)
            y: slider1.topPadding + slider1.availableHeight / 2 - height / 2
            implicitWidth: 26
            implicitHeight: 26
            radius: 13
            color: slider1.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
    }

    Label{
        color: "#ffffff"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.topMargin: 180
        anchors.leftMargin: 30

        font.pixelSize: 16
        text: 'Third Joint Cotroller'
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    Slider {
        id: slider2
        from: -90
        to: 90
        snapMode: Slider.SnapOnRelease
        value: 0

        anchors.top: parent.top
        anchors.topMargin: 220

        background: Rectangle {
            x: slider2.leftPadding
            y: slider2.topPadding + slider2.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: slider2.availableWidth
            height: implicitHeight
            radius: 2
            color: "#bdbebf"

            Rectangle {
                width: slider2.visualPosition * parent.width
                height: parent.height
                color: "#21be2b"
                radius: 2
            }
        }

        handle: Rectangle {
            x: slider2.leftPadding + slider2.visualPosition * (slider2.availableWidth - width)
            y: slider2.topPadding + slider2.availableHeight / 2 - height / 2
            implicitWidth: 26
            implicitHeight: 26
            radius: 13
            color: slider2.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
    }

    Slider {
        id: slider3
        from: -90
        to: 90
        snapMode: Slider.SnapOnRelease
        value: 0

        anchors.top: parent.top
        anchors.topMargin: 220

        background: Rectangle {
            x: slider3.leftPadding
            y: slider3.topPadding + slider3.availableHeight / 2 - height / 2
            implicitWidth: 200
            implicitHeight: 4
            width: slider3.availableWidth
            height: implicitHeight
            radius: 2
            color: "#bdbebf"

            Rectangle {
                width: slider3.visualPosition * parent.width
                height: parent.height
                color: "#21be2b"
                radius: 2
            }
        }

        handle: Rectangle {
            x: slider3.leftPadding + slider3.visualPosition * (slider3.availableWidth - width)
            y: slider3.topPadding + slider3.availableHeight / 2 - height / 2
            implicitWidth: 26
            implicitHeight: 26
            radius: 13
            color: slider3.pressed ? "#f0f0f0" : "#f6f6f6"
            border.color: "#bdbebf"
        }
    }
}
