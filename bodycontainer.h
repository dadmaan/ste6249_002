#ifndef BODYCONTAINER_H
#define BODYCONTAINER_H


// gmlib
#include <parametrics/surfaces/gmpcylinder.h>
#include <parametrics/surfaces/gmpsphere.h>
//stl
#include <vector>

//namespace jointsCordinate{
//    GMlib::Vector<float,3> joint0   (0.0f, 0.0f, 0.5f);
//    GMlib::Vector<float,3> joint1   (0.0f, 0.0f, 0.9f);
//    GMlib::Vector<float,3> joint2   (0.0f, 0.0f, 0.9f);
//    GMlib::Vector<float,3> pen_ball (0.0f, 0.0f, 2.0f);
//}

//*********************** CLASS Cylinder ***********************
class TestCylinder : public GMlib::PCylinder<float> {
public:
  using PCylinder::PCylinder;

    TestCylinder();
    ~TestCylinder() override {}

    bool rotate_flag = false;
    bool square = false;
    double rotation_angle = 0.0;
    double current_angle = 0.0;
    double i = 0.0;

    void localSimulate(double dt) override{
        if(rotate_flag)
        {
            if(rotation_angle > 0){
                this->rotate(GMlib::Angle(0.017453), GMlib::Point<float,3>(0.0,0.0,1.0));
                i+=0.017453;
                if(i>=rotation_angle){
                    rotate_flag = false;
                    i = 0;
                }
            }
            if(rotation_angle < 0){
                this->rotate(GMlib::Angle(-0.017453), GMlib::Point<float,3>(0.0,0.0,1.0));
                i-=0.017453;
                if(i<=rotation_angle){
                    rotate_flag = false;
                    i = 0;
                }
            }
        }
    }
};

//*********************** CLASS Sphere ***********************
class TestSphere : public GMlib::PSphere<float> {
public:
  using PSphere::PSphere;

    TestSphere();
   ~TestSphere() override {}

    bool rotate_flag = false;
    double rotation_angle = 0.0;
    double current_angle = 0.0;
    double i = 0.0;

    void localSimulate(double angle) override{
        if(rotate_flag)
        {
            if(rotation_angle > 0){
                this->rotate(GMlib::Angle(0.017453), GMlib::Point<float,3>(0.0,1.0,0.0));
                i+=0.017453;
                if(i>=rotation_angle){
                    rotate_flag = false;
                    i = 0;
                }
            }
            if(rotation_angle < 0){
                this->rotate(GMlib::Angle(-0.017453), GMlib::Point<float,3>(0.0,1.0,0.0));
                i-=0.017453;
                if(i<=rotation_angle){
                    rotate_flag = false;
                    i = 0;
                }
            }
        }
    }
}; // END class TestSphere
#endif // BODYCONTAINER_H
