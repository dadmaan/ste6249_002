#include "computation.h"
#include <math.h>
#include <vector>
#include <algorithm>

ComputationModel::ComputationModel()
{

}

void ComputationModel::init(TestCylinder *bc, TestCylinder *link1, TestCylinder *link2, TestCylinder *link3, TestCylinder *pen_body,
                            TestSphere *js1, TestSphere *js2, TestSphere *js3, TestSphere *pen_ball, bool debug)
{

    this->baseMat   = bc->getMatrix();
    this->joint1Mat = js1->getMatrix();
    this->joint2Mat = js2->getMatrix();
    this->joint3Mat = js3->getMatrix();

    this->link1Mat = link1->getMatrix();
    this->link2Mat = link2->getMatrix();
    this->link3Mat = link3->getMatrix();
    this->linkEMat = pen_body->getMatrix();

    this->endEffectorMatrix = pen_ball->getMatrix();

    this->basePos   = bc->getGlobalPos();
    this->joint1Pos = js1->getGlobalPos();
    this->joint2Pos = js2->getGlobalPos();
    this->joint3Pos = js3->getGlobalPos();
    this->endEffectorPosition = pen_ball->getGlobalPos();

    this->baseAngle = bc->current_angle;
    this->joint1Angle = js1->current_angle;
    this->joint2Angle = js2->current_angle;
    this->joint3Angle = js3->current_angle;

    this->initConfig = GMlib::Vector<float,4> (this->baseAngle,
                                               this->joint1Angle,
                                               this->joint2Angle,
                                               this->joint3Angle);
    this->deltaConfig = 0.0;
    this->deltaPosition = 0.0;
    this->currentConfig = this->initConfig;
    this->setAlpha();
    this->debug = debug;
}

GMlib::Vector<float,4> ComputationModel::computeInverseKinematic(GMlib::Vector<float,3> targetPos)
{

    std::vector<double> threshold;
        this->targetPosition = targetPos;
        this->count = 1;
        double e;
        if(!debug)IKsummary();

        do{
            if (debug)IKsummary();

            this->deltaConfig = this->getDelta();
            this->deltaConfig *= this->alpha; // dO*h
            this->initConfig += this->deltaConfig; // T=O+dO*h

            auto last_endEffectorPosition =endEffectorPosition;
            this->rotateJoints(this->initConfig);
            this->computeForwardKinematic();
            if(last_endEffectorPosition == endEffectorPosition)
                break;

            this->currentConfig = this->initConfig;
            this->count++;
            e = double((this->targetPosition - this->endEffectorPosition).getLength());
            threshold.push_back(e);
        }while( e > 4.5);

        IKsummary();
        std::cout<<"e = "<<e<<std::endl;
        std::cout<<"Min Threshold = "<<*min_element(threshold.begin(),threshold.end())<<std::endl;

    return this->currentConfig;
}

GMlib::Point<float,4> ComputationModel::getDelta()
{
    auto Jt = this->getJacobianTranspose();
    this->deltaPosition = this->targetPosition - this->endEffectorPosition;// V=T-E
    auto delta = Jt * deltaPosition;
    return delta;
}

GMlib::Matrix<float,4,3> ComputationModel::getJacobianTranspose()
{
    auto J_A = baseRotAxis ^ (this->endEffectorPosition - this->basePos);
    auto J_B = jointRotAxis ^ (this->endEffectorPosition - this->joint1Pos);
    auto J_C = jointRotAxis ^ (this->endEffectorPosition - this->joint2Pos);
    auto J_D = jointRotAxis ^ (this->endEffectorPosition - this->joint3Pos);

    GMlib::Matrix<float,3,4> jacobianMatrix;
    jacobianMatrix.setCol(J_A, 0);
    jacobianMatrix.setCol(J_B, 1);
    jacobianMatrix.setCol(J_C, 2);
    jacobianMatrix.setCol(J_D, 3);

    GMlib::Matrix<float,4,3> j_transpose_matrix;
    j_transpose_matrix.setRow(jacobianMatrix.getCol(0),0);
    j_transpose_matrix.setRow(jacobianMatrix.getCol(1),1);
    j_transpose_matrix.setRow(jacobianMatrix.getCol(2),2);
    j_transpose_matrix.setRow(jacobianMatrix.getCol(3),3);

    return j_transpose_matrix;
}

void ComputationModel::rotateJoints(GMlib::Vector<float, 4> angles)
{
    auto ra = this->currentConfig - angles;

    GMlib::HqMatrix<float,3> _matrix;
    GMlib::Vector<float,3> axis,li;
    GMlib::Point<float,3> u,v;

    // Base
//    if(isLegal('0',this->baseAngle, double(ra[0]))){
        if (debug){std::cout<<"-------------------------------------" << std::endl;std::cout<<"Base:"<< std::endl;std::cout<<this->baseMat<< std::endl;}
        axis = baseRotAxis;
        li = axis.getLinIndVec();
        u = li ^ axis;
        v = u ^ axis;
        this->baseMat.rotate(double(ra[0]),u,v,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
        if (debug)std::cout<<this->baseMat<< std::endl;
//    }

    // First Joint
//    if(isLegal('1',this->joint1Angle, double(ra[1]))){
        if (debug){std::cout<<"-------------------------------------" << std::endl;std::cout<<"First Joint:"<< std::endl;std::cout<<this->joint1Mat<< std::endl;}
        axis = jointRotAxis;
        li = axis.getLinIndVec();
        u = li ^ axis;
        v = u ^ axis;
        this->joint1Mat.rotate(double(ra[1]),u,v,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
        if (debug)std::cout<<this->joint1Mat<< std::endl;
//    }

    // Second Joint
//    if(isLegal('2',this->joint2Angle, double(ra[2]))){
        if (debug){std::cout<<"-------------------------------------" << std::endl;std::cout<<"Second Joint:"<< std::endl;std::cout<<this->joint2Mat<< std::endl;}
        this->joint2Mat.rotate(double(ra[2]),u,v,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
        if (debug)std::cout<<this->joint2Mat<< std::endl;
//    }

    // Third Joint
//    if(isLegal('3',this->joint3Angle, double(ra[3]))){
        if (debug){std::cout<<"-------------------------------------" << std::endl;std::cout<<"Third Joint:"<< std::endl;std::cout<<this->joint3Mat<< std::endl;}
        this->joint3Mat.rotate(double(ra[3]),u,v,GMlib::Vector<float,3>(0.0f, 0.0f, 0.0f));
        if (debug)std::cout<<this->joint3Mat<< std::endl;
//    }

//    updateAngles(angles);
}

void ComputationModel::updateAngles(GMlib::Vector<float, 4> Angles)
{
    this->baseAngle = double(Angles[0]);
    this->joint1Angle = double(Angles[1]);
    this->joint2Angle = double(Angles[2]);
    this->joint3Angle = double(Angles[3]);
}


GMlib::HqMatrix<float,3> ComputationModel::computeForwardKinematic()
{

    GMlib::Point<float,3> p;

    GMlib::HqMatrix<float,3> base = this->baseMat;
    GMlib::HqMatrix<float,3> joint1 = this->joint1Mat;
    GMlib::HqMatrix<float,3> joint2 = this->joint2Mat;
    GMlib::HqMatrix<float,3> joint3 = this->joint3Mat;
    GMlib::HqMatrix<float,3> end_effector = this->endEffectorMatrix;

    if (debug){std::cout<<"Base:"<< std::endl;std::cout<<base<< std::endl;}

    GMlib::HqMatrix<float,3> j1 = base * joint1;
    this->joint1Pos = GMlib::Point<float,3>(j1[0][3],j1[1][3],j1[2][3]);
    if (debug)std::cout<<j1<< std::endl;

    GMlib::HqMatrix<float,3> j2 = j1 * link1Mat * joint2;
    this->joint2Pos = GMlib::Point<float,3>(j2[0][3],j2[1][3],j2[2][3]);
    if (debug)std::cout<<j2<< std::endl;

    GMlib::HqMatrix<float,3> j3 = j2 * link2Mat * joint3;
    this->joint3Pos = GMlib::Point<float,3>(j3[0][3],j3[1][3],j3[2][3]);
    if (debug)std::cout<<j3<< std::endl;

    if (debug){std::cout<<"-------------------------------------" << std::endl;std::cout<<"Etip:"<< std::endl;std::cout<<end_effector<< std::endl;}
    GMlib::HqMatrix<float,3> Etip = j3 * link3Mat * linkEMat * end_effector;
    this->endEffectorMatrix = Etip;
    this->endEffectorPosition = GMlib::Point<float,3>(Etip[0][3],Etip[1][3],Etip[2][3]);
    if (debug){std::cout<<this->endEffectorMatrix<< std::endl;std::cout<<"#####################################" << std::endl;}

    return Etip;
}

bool ComputationModel::isLegal(char joint, int current_angle, int rotation_angle)
{
    auto ca = current_angle;
    auto rotaion_degree = rotation_angle;
    auto limit = 5*M_PI/180;


    switch(joint){
    case 0:
        if(ca >= M_PI){
            this->baseAngle = M_PI;
            if(rotaion_degree < 0 and rotaion_degree > -limit)return true;
            return false;
        }
        else if(ca <= -M_PI){
            this->baseAngle = -M_PI;
            if(rotaion_degree > 0 and rotaion_degree < limit)return true;
            return false;
        };
        break;
    case 1:
        if(ca >= M_PI/2){
            this->joint1Angle = M_PI/2;
            if(rotaion_degree < 0 and rotaion_degree > -limit)return true;
            return false;
        }
        else if(ca <= -M_PI/2){
            this->joint1Angle = -M_PI/2;
            if(rotaion_degree > 0 and rotaion_degree < limit)return true;
            return false;
        }
        break;
    case 2:
        if(ca >= M_PI/2){
            this->joint2Angle = M_PI/2;
            if(rotaion_degree < 0 and rotaion_degree > -limit)return true;
            return false;
        }
        else if(ca <= -M_PI/2){
            this->joint2Angle = -90;
            if(rotaion_degree > 0 and rotaion_degree < limit)return true;
            return false;
        }
        break;
    case 3:
        if(ca >= M_PI/2){
            this->joint3Angle = M_PI/2;
            if(rotaion_degree < 0 and rotaion_degree > -limit)return true;
            return false;
        }
        else if(ca <= -M_PI/2){
            this->joint3Angle = -M_PI/2;
            if(rotaion_degree > 0 and rotaion_degree < limit)return true;
            return false;
        }
        break;
    }
    return 0;
}

void ComputationModel::setAlpha()
{
    auto e = this->targetPosition - this->endEffectorPosition;

    auto J_A = baseRotAxis ^ (this->endEffectorPosition - this->basePos);
    auto J_B = jointRotAxis ^ (this->endEffectorPosition - this->joint1Pos);
    auto J_C = jointRotAxis ^ (this->endEffectorPosition - this->joint2Pos);
    auto J_D = jointRotAxis ^ (this->endEffectorPosition - this->joint3Pos);

    GMlib::Matrix<float,3,4> j;
    j.setCol(J_A, 0);
    j.setCol(J_B, 1);
    j.setCol(J_C, 2);
    j.setCol(J_D, 3);

    GMlib::Matrix<float,4,3> jt;
    jt.setRow(j.getCol(0),0);
    jt.setRow(j.getCol(1),1);
    jt.setRow(j.getCol(2),2);
    jt.setRow(j.getCol(3),3);

    auto nominator = e*(j*jt*e);//<e,j*jt*e>
    auto denominator = (j *jt*e)*(j *jt*e);//<j*jt*e,j*jt*e>
    this->alpha = nominator/denominator;
    std::cout<<"Alpha = "<<this->alpha<<std::endl;
}

void ComputationModel::IKsummary()
{
    std::cout<<"Etip = " << this->endEffectorPosition<< std::endl;
    std::cout<<"Target = " << this->targetPosition<< std::endl;
    std::cout<<"Base = " << this->basePos<< std::endl;
    std::cout<<"J1 = " << this->joint1Pos<< std::endl;
    std::cout<<"J2 = " << this->joint2Pos<< std::endl;
    std::cout<<"J3 = " << this->joint3Pos<< std::endl;
    std::cout<<"initial Config : " << this->initConfig << std::endl;
    std::cout<<"Delta Config : " << this->deltaConfig << std::endl;
    std::cout<<"Delta Position : " << this->deltaPosition << std::endl;
    std::cout<<"Threshold : " << (this->endEffectorPosition - this->targetPosition).getLength() << std::endl;
    std::cout<<"Number of Iterations : " << this->count << std::endl;
    std::cout<<"-------------------------------------" << std::endl;
}

GMlib::HqMatrix<float,3> ComputationModel::getTransformationMatrix( const double d,
                                                                    const double theta,
                                                                    const double a,
                                                                    const double alpha)
{
    GMlib::HqMatrix<float,3> t_matrix;

    t_matrix[0][0] = std::cos(theta);
    if (t_matrix[0][0] < 1e-7) t_matrix[0][0] = 0.0;
    t_matrix[0][1] =-std::cos(alpha) * std::sin(theta);
    if (t_matrix[0][1] < 1e-7) t_matrix[0][1] = 0.0;
    t_matrix[0][2] = std::sin(alpha) * std::sin(theta);
    if (t_matrix[0][2] < 1e-7) t_matrix[0][2] = 0.0;
    t_matrix[0][3] = a               * std::cos(theta);
    if (t_matrix[0][3] < 1e-7) t_matrix[0][3] = 0.0;

    t_matrix[1][0] = std::sin(theta);
    if (t_matrix[1][0] < 1e-7) t_matrix[1][0] = 0.0;
    t_matrix[1][1] = std::cos(alpha) * std::cos(theta);
    if (t_matrix[1][1] < 1e-7) t_matrix[1][1] = 0.0;
    t_matrix[1][2] =-std::sin(alpha) * std::cos(alpha);
    if (t_matrix[1][2] < 1e-7) t_matrix[1][2] = 0.0;
    t_matrix[1][3] = a               * std::sin(theta);
    if (t_matrix[1][3] < 1e-7) t_matrix[1][3] = 0.0;

    t_matrix[2][0] = 0;
    t_matrix[2][1] = std::sin(alpha);
    if (t_matrix[2][1] < 1e-7) t_matrix[2][1] = 0.0;
    t_matrix[2][2] = std::cos(alpha);
    if (t_matrix[2][2] < 1e-7) t_matrix[2][2] = 0.0;
    t_matrix[2][3] = d;

    t_matrix[3][0] = 0;
    t_matrix[3][1] = 0;
    t_matrix[3][2] = 0;
    t_matrix[3][3] = 1;

    std::cout << "Transformation Matrix:" << std::endl << t_matrix << std::endl;
    return t_matrix;
}
