#ifndef SCENARIO_H
#define SCENARIO_H

#include "application/gmlibwrapper.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <string>
#include <chrono>

//#include <parametrics/surfaces/gmpsphere.h>

// qt
#include <QObject>
#include <vector>
class Scenario : public GMlibWrapper {
    Q_OBJECT
public:
    using GMlibWrapper::GMlibWrapper;

    void        initializeScenario() override;
    void        cleanupScenario() override;
    void        rotate(double sv, char s);
    double      desirAngle(double sv, TestSphere* j);
    double      desirAngle(double sv, TestCylinder* b);
    void        armController();
    void        setTargetPos();
    void        updateJoints(const GMlib::Vector<float,4> angles);
    void        lynxMotionController(int joint, int angle, int velocity);
    void        initDraw();
    void        drawSqaure();
    void        drawCircle();
    void        defaultPos();
    void        delay(int number_of_seconds);


    TestCylinder* bc;
    TestCylinder* link1;
    TestCylinder* link2;
    TestCylinder* link3;
    TestCylinder* pen_body;
    TestSphere* js1;
    TestSphere* js2;
    TestSphere* js3;
    TestSphere* pen_ball;
    TestSphere* debug;

    GMlib::Point<float, 3> targetPos;
    bool draw_flag = false;
    bool node1 = true;
    bool node2 = false;
    bool node3 = false;
    bool node4 = false;

    QSerialPort _lynxmotion;

public slots:
    void    callDefferedGL();

};

#endif // SCENARIO_H
