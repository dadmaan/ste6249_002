#ifndef COMPUTATION_H
#define COMPUTATION_H

#include "bodycontainer.h"

class ComputationModel
{
public:
                                ComputationModel();
    void                        init(TestCylinder *bc, TestCylinder *link1, TestCylinder *link2, TestCylinder *link3, TestCylinder *pen_body,
                                     TestSphere *js1, TestSphere *js2, TestSphere *js3, TestSphere *pen_ball,bool debug);
    GMlib::HqMatrix<float,3>    computeForwardKinematic ();
    GMlib::HqMatrix<float,3>    getTransformationMatrix (const double d,
                                                        const double theta,
                                                        const double a,
                                                        const double alpha);
    GMlib::Vector<float,4>      computeInverseKinematic (GMlib::Vector<float,3> targetPos);
    GMlib::Point<float,4>       getDelta();
    GMlib::Matrix<float,4,3>    getJacobianTranspose();
    void                        rotateJoints(GMlib::Vector<float,4> Angles);
    void                        updateAngles(GMlib::Vector<float,4> Angles);
    void                        IKsummary();
    bool                        isLegal(char joint, int current_angle, int rotation_angle);
    void                        setAlpha();

private:
    bool debug;
    int count;
    float alpha;// sim step; from 0.01 to 0.000000001

    GMlib::Vector<float,4> initConfig;
    GMlib::Vector<float,4> currentConfig;
    GMlib::Point<float,4> deltaConfig;
    GMlib::Vector<float,3> deltaPosition;

    double baseAngle;
    double joint1Angle;
    double joint2Angle;
    double joint3Angle;

    GMlib::HqMatrix<float,3> endEffectorMatrix;
    GMlib::HqMatrix<float,3> baseMat;
    GMlib::HqMatrix<float,3> joint1Mat;
    GMlib::HqMatrix<float,3> joint2Mat;
    GMlib::HqMatrix<float,3> joint3Mat;
    GMlib::HqMatrix<float,3> link1Mat;
    GMlib::HqMatrix<float,3> link2Mat;
    GMlib::HqMatrix<float,3> link3Mat;
    GMlib::HqMatrix<float,3> linkEMat;

    GMlib::Point<float,3> endEffectorPosition;
    GMlib::Point<float,3> targetPosition;
    GMlib::Point<float,3> basePos;
    GMlib::Point<float,3> joint1Pos;
    GMlib::Point<float,3> joint2Pos;
    GMlib::Point<float,3> joint3Pos;

public:
    GMlib::Vector<float,3> baseRotAxis = GMlib::Vector<float,3>(0.0f, 0.0f, 1.0f);
    GMlib::Vector<float,3> jointRotAxis = GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f);
};

#endif // COMPUTATION_H
